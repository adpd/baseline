# Baseline ADPD

Get AD patient data

## Usage

``` bash
git clone git@gitlab.com:adpd/baseline.git
# install Python dependencies
./baseline/install_LAMID_PREVENT-AD

mkdir data 
cd data
python ../baseline/LAMID_PREVENT-AD.py -o ./ -t bids -m t1w,dwi65 -v NAPEN00
```



## Licence

MIT
